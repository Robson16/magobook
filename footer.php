</main>

<footer class="bg-black bg-cover" style="background-image: url(<?php echo get_template_directory_uri() . '/assets/image/bg-little-dots.png'; ?>)">
    <div class="container py-5">
        <div class="row ">
            <?php if ( is_active_sidebar( 'magobook-sidebar-footer-1' ) ) : ?>
                <div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center py-3">
                    <?php dynamic_sidebar( 'magobook-sidebar-footer-1' ); ?>
                </div>
                <!-- /.col -->
            <?php endif; ?>

            <?php if (is_active_sidebar( 'magobook-sidebar-footer-2' ) ) : ?>
                <div class="col-12 col-md-6 d-flex flex-column justify-content-center align-items-center py-3">
                    <?php dynamic_sidebar( 'magobook-sidebar-footer-2' ); ?>
                </div>
                <!-- /.col -->
            <?php endif; ?>
        </div>
        <!-- /.row -->
    </div>

    <div class="copyright container py-3">
        <div class="row text-white">
            <div class="col-12 col-md-6">
                <p class=" text-center text-md-left m-0">
                    &#9400; <?php echo date('Y') . ' ' . get_bloginfo('name') . ' - '; ?> Todos os direitos reservados.
                </p>
            </div>
            <!-- /.col -->
            <div class="col-12 col-md-6">
                <p class="text-center text-md-right m-0">
                    Design por <a href="https://www.stratesign.com.br/" target="_blank" rel="noopener">Stratesign</a>
                </p>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container -->
</footer>

<?php wp_footer(); ?>

</body>

</html>