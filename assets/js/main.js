// Theme Main Js File
jQuery(document).ready(function ($) {

    /**
     * Normalize Bootstrap Carousel Item Heights
     * https://snook.ca/archives/javascript/normalize-bootstrap-carousel-heights
     */
    function normalizeSlideHeights() {
        $('.carousel').each(function () {
            var items = $('.carousel-item', this);

            // reset the height
            items.css('min-height', 0);

            // set the height
            var maxHeight = Math.max.apply(null, items.map(function () {
                return $(this).outerHeight()
            }).get());

            items.css('min-height', maxHeight + 'px');
        })
    }


    $("main").css("margin-top", $("#navbar").outerHeight());
    $(window).on('load resize orientationchange', normalizeSlideHeights);
});