<?php

/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme magobook for publication on WordPress.org
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */
require_once get_template_directory() . '/includes/classes/class-tgm-plugin-activation.php';

add_action('tgmpa_register', 'magobook_register_required_plugins');

function magobook_register_required_plugins() {
    $plugins = array(
        // This is how to include a plugin from the WordPress Plugin Repository.
        array(
            'name' => 'Kirki Customizer Framework',
            'slug' => 'kirki',
            'required' => true,
        ),
        array(
            'name' => 'Advanced Custom Fields',
            'slug' => 'advanced-custom-fields',
            'required' => true,
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
            'required' => false,
        ),
        array(
            'name' => 'Flamingo',
            'slug' => 'flamingo',
            'required' => false,
        ),
    );

    $config = array(
        'id' => 'magobook', // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '', // Default absolute path to bundled plugins.
        'menu' => 'tgmpa-install-plugins', // Menu slug.
        'has_notices' => true, // Show admin notices or not.
        'dismissable' => true, // If false, a user cannot dismiss the nag message.
        'dismiss_msg' => '', // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false, // Automatically activate plugins after installation or not.
        'message' => '', // Message to output right before the plugins table.
        'strings' => array(
            'page_title' => __('Install Required Plugins', 'magobook'),
            'menu_title' => __('Install Plugins', 'magobook'),
            'installing' => __('Installing Plugin: %s', 'magobook'),
            'updating' => __('Updating Plugin: %s', 'magobook'),
            'oops' => __('Something went wrong with the plugin API.', 'magobook'),
            'notice_can_install_required' => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'magobook'),
            'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'magobook'),
            'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'magobook'),
            'notice_ask_to_update_maybe' => _n_noop('There is an update available for: %1$s.', 'There are updates available for the following plugins: %1$s.', 'magobook'),
            'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'magobook'),
            'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'magobook'),
            'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins', 'magobook'),
            'update_link' => _n_noop('Begin updating plugin', 'Begin updating plugins', 'magobook'),
            'activate_link' => _n_noop('Begin activating plugin', 'Begin activating plugins', 'magobook'),
            'return' => __('Return to Required Plugins Installer', 'magobook'),
            'plugin_activated' => __('Plugin activated successfully.', 'magobook'),
            'activated_successfully' => __('The following plugin was activated successfully:', 'magobook'),
            'plugin_already_active' => __('No action taken. Plugin %1$s was already active.', 'magobook'),
            'plugin_needs_higher_version' => __('Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'magobook'),
            'complete' => __('All plugins installed and activated successfully. %1$s', 'magobook'),
            'dismiss' => __('Dismiss this notice', 'magobook'),
            'notice_cannot_install_activate' => __('There are one or more required or recommended plugins to install, update or activate.', 'magobook'),
            'contact_admin' => __('Please contact the administrator of this site for help.', 'magobook'),
            'nag_type' => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
        ),
    );

    tgmpa($plugins, $config);
}
