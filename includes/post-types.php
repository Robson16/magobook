<?php

// Register Custom Post Type
function magobook_testimonials() {

    $labels = array(
        'name'                  => _x( 'Depoimentos', 'Post Type General Name', 'magobook' ),
        'singular_name'         => _x( 'Depoimento', 'Post Type Singular Name', 'magobook' ),
        'menu_name'             => __( 'Depoimentos', 'magobook' ),
        'name_admin_bar'        => __( 'Depoimentos', 'magobook' ),
        'archives'              => __( 'Arquivos do Item', 'magobook' ),
        'attributes'            => __( 'Atributos do Item', 'magobook' ),
        'parent_item_colon'     => __( 'Item Pai:', 'magobook' ),
        'all_items'             => __( 'Todos os Items', 'magobook' ),
        'add_new_item'          => __( 'Adicionar Novo Item', 'magobook' ),
        'add_new'               => __( 'Adicionar Novo', 'magobook' ),
        'new_item'              => __( 'Novo Item', 'magobook' ),
        'edit_item'             => __( 'Editar Item', 'magobook' ),
        'update_item'           => __( 'Atualizar Item', 'magobook' ),
        'view_item'             => __( 'Ver Item', 'magobook' ),
        'view_items'            => __( 'Ver Itens', 'magobook' ),
        'search_items'          => __( 'Procurar Item', 'magobook' ),
        'not_found'             => __( 'Não encontrado', 'magobook' ),
        'not_found_in_trash'    => __( 'Não encontrado na Lixeira', 'magobook' ),
        'featured_image'        => __( 'Imagem Destacada', 'magobook' ),
        'set_featured_image'    => __( 'Configurar imagem destacada', 'magobook' ),
        'remove_featured_image' => __( 'Remover imagem destacada', 'magobook' ),
        'use_featured_image'    => __( 'Usar como imagem destacada', 'magobook' ),
        'insert_into_item'      => __( 'Inserir no item', 'magobook' ),
        'uploaded_to_this_item' => __( 'Enviado para este item', 'magobook' ),
        'items_list'            => __( 'Lista do item', 'magobook' ),
        'items_list_navigation' => __( 'Navegação de lista dos itens', 'magobook' ),
        'filter_items_list'     => __( 'Filtrar lista de itens', 'magobook' ),
    );
    $args = array(
        'label'                 => __( 'Depoimento', 'magobook' ),
        'description'           => __( 'Depoimentos de clientes', 'magobook' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'author', 'thumbnail' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-quote',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'testimonials', $args );

}
add_action( 'init', 'magobook_testimonials', 0 );