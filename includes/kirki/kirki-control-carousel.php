<?php

Kirki::add_section( 'section_carousel', array(
    'title' => esc_html__( 'Carrossel da página inicial', 'magobook' ),
    'priority' => 160,
));

Kirki::add_field( 'magobook_kirki_config', [
    'type' => 'repeater',
    'label' => esc_html__( 'Slides', 'magobook' ),
    'section' => 'section_carousel',
    'priority' => 10,
    'row_label' => [
        'type' => 'field',
        'value' => esc_html__( 'Slide', 'magobook' ),
        'field' => 'link_text',
    ],
    'button_label' => esc_html__( 'Add novo', 'magobook' ),
    'settings' => 'setting_carousel',
    'fields' => [
        'slide_desktop' => [
            'type' => 'image',
            'label' => esc_html__( 'Slide', 'magobook' ),
            'description' => esc_html__( 'Versão para dispositivos grandes', 'magobook' ),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
        'slide_mobile' => [
            'type' => 'image',
            'label' => esc_html__( 'Slide Mobile', 'magobook' ),
            'description' => esc_html__( 'Versão para dispositivos móveis', 'magobook' ),
            'choices' => [
                'save_as' => 'id',
            ],
        ],
    ]
]);
