<?php

Kirki::add_section( 'section_contacts', array(
    'title' => esc_html__( 'Contatos', 'magobook' ),
    'priority' => 160,
));

Kirki::add_field( 'magobook_kirki_config', [
    'type' => 'text',
    'settings' => 'setting_form',
    'label' => __( 'Formulário', 'magobook' ),
    'description' => __( 'Shortcode do formulário de contato', 'magobook' ),
    'section' => 'section_contacts',
    'default' => '',
    'priority' => 10,
]);