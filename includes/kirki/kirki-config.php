<?php

function magobook_kirki() {

    if ( class_exists( 'Kirki' ) ) {

        Kirki::add_config( 'magobook_kirki_config', array (
            'capability' => 'edit_theme_options',
            'option_type' => 'theme_mod',
        ) );       
        
        require_once get_template_directory() . '/includes/kirki/kirki-control-carousel.php';
        require_once get_template_directory() . '/includes/kirki/kirki-control-contacts.php';

    }

}

add_action( 'customize_register', 'magobook_kirki' );