<?php

/**
 * The template for displaying all single posts
 *
 */
get_header();
?>
<main>
    <div class="container pt-5">
        <?php
        while (have_posts()) {
            the_post();
            get_template_part( 'partials/content/content' );
        }
        ?>
    </div>
    <!-- /.container -->

    <div class="bg-dark text-white">
        <div class="container">
            <?php 
            if ( comments_open() || get_comments_number() ) comments_template(); 

            echo get_template_part( 'partials/sections/section', 'contact' );
            ?>
        </div>
        <!-- /.container -->
    </div>
</main>

<?php
get_footer();
