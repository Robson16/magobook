<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <nav id="navbar" role="navigation" class="navbar navbar-dark navbar-expand-lg fixed-top bg-dark p-md-0">
        <div class="container">
            <a class="navbar-brand py-3" href="<?php echo home_url(); ?>">
                <?php if (has_custom_logo()) : ?>
                    <img class="img-fluid" src="<?php echo wp_get_attachment_image_src(get_theme_mod('custom_logo'), 'full')[0]; ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('name'); ?>">
                <?php else : ?>
                    <h1><?php echo get_bloginfo('name'); ?></h1>
                <?php endif; ?>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-navbar-collapse" aria-controls="bs-navbar-collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <?php
            wp_nav_menu(
                array(
                    'theme_location' => 'main_menu',
                    'depth' => 2,
                    'container' => 'div',
                    'container_class' => 'collapse navbar-collapse',
                    'container_id' => 'bs-navbar-collapse',
                    'menu_class' => 'nav navbar-nav ml-auto',
                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                    'walker' => new WP_Bootstrap_Navwalker()
                )
            );
            ?>
        </div>
    </nav>