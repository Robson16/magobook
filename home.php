<?php get_header(); ?>

<main class="bg-dark text-white">
    <!-- BEGIN BLOG -->  
    <section class="latest-posts">
            <div class="container pt-5">
                <div class="row">
                    <div class="col-12">
                        <h1 class="title text-center"><?php _e( 'Blog do Mago', 'magobook' ); ?></h1>
                    </div>
                    <!-- /.col -->
                    <div class="col-12">
                        <?php
                        while ( have_posts() ) {
                            the_post();
                            get_template_part( 'partials/content/content', 'excerpt' );
                        }
                        ?>
                    </div>
                    <!-- /.col -->
                    <div class="col-12 d-flex justify-content-center order-3 pb-5">
                        <?php echo bootstrap_pagination(); ?>
                    </div> 
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
    </section>
    <!-- BEGIN BLOG -->

    <?php get_template_part( 'partials/sections/section', 'contact' ); ?>

</main>

<?php

get_footer();
