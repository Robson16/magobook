<?php

/** 
 * Configuração do tema
 */
function magobook_config() {
    // Habilitando suporte à tradução
    $textdomain = 'magobook';
    load_theme_textdomain( $textdomain, get_stylesheet_directory() . '/languages/' );
    load_theme_textdomain( $textdomain, get_template_directory() . '/languages/' );

    // Menu registration
    register_nav_menus(array(
        'main_menu' => __( 'Main Menu', 'magobook' ),
    ));

    // Adicionando theme suportes
    add_theme_support( 'title-tag' );
    add_theme_support( 'custom-logo', array(
        'height' => 95,
        'width' => 375,
        'flex-height' => true,
        'flex-width' => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ));
    add_theme_support( 'responsive-embeds' );
    add_theme_support( 'post-thumbnails' );

    // Esconde a barra de administrador se for mobile
    if ( wp_is_mobile() ) {
        show_admin_bar( false );
    }
}

add_action('after_setup_theme', 'magobook_config');

/** 
 * Carregamento de Scripts e Folhas de Estilo
 */
function magobook_scripts() {
    // CSS
    wp_enqueue_style( 'font-bebas-neue', '//fonts.googleapis.com/css?family=Bebas+Neue&display=swap', array(), '1.0', 'all' );
    wp_enqueue_style( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css', array(), '4.4.1', 'all' );
    wp_enqueue_style( 'magobook-style', get_template_directory_uri() . '/assets/css/frontend/frontend-style.css', array( 'bootstrap', 'font-bebas-neue' ), wp_get_theme()->get( 'Version' ) );
    // Js
    wp_deregister_script( 'jquery');
    wp_register_script( 'jquery', '//ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js', false, '3.4.1' );
    wp_enqueue_script( 'jquery');
    wp_enqueue_script( 'comment-reply' );
    wp_enqueue_script( 'popper', '//cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js', array( 'jquery' ), '4.4.1', true );
    wp_enqueue_script( 'bootstrap', '//stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array( 'jquery', 'popper' ), '4.4.1', true );
    wp_enqueue_script( 'fontawesome', '//kit.fontawesome.com/dd70604887.js', array(), '5.9.0', true );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), '1.0', true );
}

add_action('wp_enqueue_scripts', 'magobook_scripts');

/**
 * Registro de áreas de widgets.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function magobook_sidebars() {

    // Arguments used in all register_sidebar() calls.
    $shared_args = array(
        'before_title' => '<h4 class="widget-title">',
        'after_title' => '</h4>',
        'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
        'after_widget' => '</div></div>',
    );

    // Rodapé #1.
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Rodapé #1', 'magobook'),
        'id' => 'magobook-sidebar-footer-1',
        'description' => __('Os widgets nesta área serão exibidos na primeira coluna no rodapé.', 'magobook'),
    ) ) );

    // Rodapé #2.
    register_sidebar( array_merge( $shared_args, array(
        'name' => __('Rodapé #2', 'magobook'),
        'id' => 'magobook-sidebar-footer-2',
        'description' => __('Os widgets nesta área serão exibidos na segunda coluna no rodapé.', 'magobook'),
    ) ) );
}

add_action('widgets_init', 'magobook_sidebars');

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 *  WordPress Bootstrap Pagination
 */
require_once get_template_directory() . '/includes/wp-bootstrap4.1-pagination.php';

/**
 *  TGM Plugin Activation
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 * Extra functions, filters, and actions for the theme
 */
require get_template_directory() . '/includes/template-functions.php';

/**
 * Custom Post-Types
 */
require_once get_template_directory() . '/includes/post-types.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';
