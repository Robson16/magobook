<?php
/*
 * Template Part para exibir que nenhuma publicação foi encontrada
 */
?>

<div class="col">
    <h3 class="text-center"><?php _e( 'Nenhum conteúdo para exibir', 'magobook' ); ?></h3>
</div>
