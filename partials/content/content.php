<div class="d-flex flex-column flex-md-row justify-content-start align-items-center align-items-md-start">
    <?php
    if (has_post_thumbnail()) :
        the_post_thumbnail( 'thumbnail', array(
            'class' => 'rounded-circle',
            'title' => get_the_title(),
            'alt' => get_the_title(),
        ) );
    else :
    ?>
        <img class="rounded-circle" src="https://via.placeholder.com/150" alt="<?php get_the_title(); ?>" title="<?php get_the_title(); ?>">
    <?php endif; ?>

    <div class="m-3">
        <header class="entry-header">
            <?php
            if ( is_single() ) {
                the_title( '<h1 class="entry-title text-color-one text-center text-md-left">', '</h1>' ); 
            } else {
                the_title( '<h2 class="entry-title text-center text-md-left">', '</h2>' ); 
            }            
            ?>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <?php the_content(); ?>
        </div><!-- .entry-content -->
    </div>
</div>
