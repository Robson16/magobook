<div class="d-flex flex-column flex-md-row justify-content-start align-items-center">
    <?php
    if (has_post_thumbnail()) : ?>
        <a href="<?php echo get_permalink() ?>" >
            <?php
                the_post_thumbnail( 'thumbnail', array(
                    'class' => 'rounded-circle',
                    'title' => get_the_title(),
                    'alt' => get_the_title(),
                ) );
            ?>
        </a>
    <?php else : ?>
        <img class="rounded-circle" src="https://via.placeholder.com/150" alt="<?php get_the_title(); ?>" title="<?php get_the_title(); ?>">
    <?php endif; ?>

    <div class="text-center text-md-left m-3">
        <header class="entry-header">
            <?php the_title( sprintf( '<h3 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
        </header><!-- .entry-header -->

        <div class="entry-content">
            <p class=""><?php echo wp_trim_words( get_the_excerpt(), 23 ); ?></p>
        </div><!-- .entry-content -->        
    </div>
</div>