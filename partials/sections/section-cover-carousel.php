<!-- BEGIN COVER -->
<?php
$slides = get_theme_mod( 'setting_carousel' );
$is_mobile = wp_is_mobile();

if ($slides) : 
?>
    <section id="cover">
        <div id="cover-carousel" class="carousel slide" data-ride="carousel">

            <?php if (sizeof($slides) > 1) : ?>
                <ol class="carousel-indicators">
                    <?php for ($x = 0; $x < sizeof($slides); $x++) : ?>
                        <li data-target="#cover-carousel" data-slide-to="<?php echo $x ?>" class="<?php if ($x === 0) echo 'active'; ?>"></li>
                    <?php endfor; ?>
                </ol>
                <!-- /.carousel-indicators -->
            <?php endif; ?>

            <div class="carousel-inner">
                <?php for ($x = 0; $x < sizeof($slides); $x++) : ?>
                    <div class="carousel-item <?php if ($x === 0) echo 'active'; ?>">
                        <img 
                            class="d-block w-100" 
                            src="<?php echo wp_get_attachment_image_src(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], 'full')[0]; ?>" 
                            alt="<?php echo get_post_meta(($is_mobile) ? $slides[$x]['slide_mobile'] : $slides[$x]['slide_desktop'], '_wp_attachment_image_alt', TRUE); ?>"
                        >
                    </div>
                <?php endfor; ?>
            </div>
            <!-- /.carousel-inner --> 

        </div>
        <!-- /#cover-corousel -->
    </section>
    <!-- /#cover -->
<?php endif; ?>
<!-- END COVER -->