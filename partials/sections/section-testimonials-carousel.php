<!-- BEGIN TESTIMONIALS CAROUSEL -->
<?php
$testimonials = new WP_Query(array(
    'post_type' => 'testimonials',
    'update_post_meta_cache' => false,
    'update_post_term_cache' => false,
    'nopaging' => true,
));

if ($testimonials->have_posts()) :
?>
    <section id="depoimentos" class="testimonials">
        <img class="big-quotes big-quotes-start" src="<?php echo get_template_directory_uri() . '/assets/image/big-quotes.png'; ?>" alt="Big Quotes">
        <div class="container">
            <div id="testimonials-carousel" class="carousel slide py-5" data-ride="carousel">
                <?php 
                $the_count = $testimonials->found_posts;
                if ( $the_count > 1 ) : 
                ?>
                    <ol class="carousel-indicators">
                        <?php for ( $x = 0; $x < $the_count; $x++ ) : ?>
                            <li data-target="#testimonials-carousel" data-slide-to="<?php echo $x ?>" class="<?php if ($x === 0) echo 'active'; ?>"></li>
                        <?php endfor; ?>
                    </ol>
                    <!-- /.carousel-indicators -->
                <?php endif; ?>
                
                <div class="carousel-inner py-5">
                    <?php 
                    $loopCount = 0;
                    while ( $testimonials->have_posts() ) : $testimonials->the_post(); 
                    ?>
                        <div class="carousel-item <?php if( $loopCount == 0 ) echo 'active'; ?>">
                            <p class="h1 text-uppercase text-center mb-5">
                                <?php echo get_the_excerpt();?>
                            </p>
                            <div class="d-flex flex-column flex-md-row justify-content-center align-items-center">
                                <?php 
                                if ( has_post_thumbnail() ): 
                                    the_post_thumbnail( 'thumbnail', array(
                                        'class' => 'rounded-circle',
                                        'title' => get_the_title(),
                                        'alt' => get_the_title(),
                                    ) );
                                else: 
                                ?>
                                    <img class="rounded-circle" src="https://via.placeholder.com/150" alt="<?php get_the_title(); ?>" title="<?php get_the_title(); ?>"> 
                                <?php endif; ?>

                                <div class="text-center text-md-left m-3">
                                    <h3 class="h1 mb-1"><?php echo get_the_title();?></h3>
                                    <h4 class="h2 text-color-two"><?php the_field( 'subtitle' ); ?></h4>
                                </div>
                            </div>
                        </div>
                        <!-- /.carousel-item -->
                    <?php
                    $loopCount++; 
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
                <!-- /.carousel-inner -->
            </div>
            <!-- /#testimonials-corousel -->
        </div>
        <!-- /.container -->
        <img class="big-quotes big-quotes-end" src="<?php echo get_template_directory_uri() . '/assets/image/big-quotes.png'; ?>" alt="Big Quotes">
    </section>
    <!-- /#testimonials -->
<?php endif; ?>
<!-- END TESTIMONIALS CAROUSEL -->