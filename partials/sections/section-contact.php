<!-- BEGIN CONTACT -->
<section id="contato" class="contact">
    <div class="container pb-5 py-lg-5">
        <div class="row">
            <div class="col-12 col-lg-4">
                <h2 class="title text-center text-lg-left"><?php _e( 'Junte-se a pessoas que recebem conteúdos exclusivos', 'magobook' ); ?></h2>
            </div>
            <!-- /.col -->
            <div class="col-12 col-lg-8">
                <?php echo do_shortcode( get_theme_mod( 'setting_form' ) ); ?>
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div>
</section>
<!-- END CONTACT -->