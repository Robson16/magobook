<?php

/**
 * Template Name: Front-Page 1
 * Template Post Type: page
 * 
 */

get_header();
?>

<main class="bg-dark text-white">
    <h1 class="d-none"><?php echo bloginfo( 'title' ); ?></h1>
    <?php echo get_template_part( 'partials/sections/section', 'cover-carousel' ); ?>
    <?php echo get_template_part( 'partials/sections/section', 'testimonials-carousel' ); ?>  
    
    <!-- BEGIN CONTENT -->
    <section class="bg-white text-body">
        <div class="container py-5">
            <?php
            while ( have_posts() ) {
                the_post();
                the_content();
            }
            ?>
        </div>
        <!-- /.container -->
    </section>    
    <!-- END CONTENT -->

    <!-- BEGIN LATEST FROM BLOG -->
    <?php 
    $latestPosts = new WP_Query( array(
        'post_type' => 'post',
        'posts_per_page' => 3,
        'update_post_meta_cache' => false,
        'update_post_term_cache' => false,
    ) );

    if ( $latestPosts->have_posts() ) :
    ?>
        <section class="latest-posts">
            <div class="container pt-5">
                <div class="row">
                    <div class="col-12 col-lg-8 order-2 order-lg-1">
                        <?php
                        while ( $latestPosts->have_posts() ) {
                            $latestPosts->the_post();
                            get_template_part( 'partials/content/content', 'excerpt' );
                        }
                        wp_reset_postdata();
                        ?>
                    </div>
                    <!-- /.col -->
                    <div class="col-12 col-lg-4 order-1 order-lg-2">
                        <h2 class="title text-center text-lg-right"><?php _e( 'Blog do Mago', 'mago' ); ?></h2>
                    </div>
                    <!-- /.col --> 
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </section>
    <?php endif; ?>
    <!-- BEGIN LATEST FROM BLOG -->

    <?php echo get_template_part( 'partials/sections/section', 'contact' ); ?>

</main>

<?php

get_footer();
